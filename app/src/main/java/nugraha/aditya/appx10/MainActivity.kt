package nugraha.aditya.appx10

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View


import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() ,View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLogin.setOnClickListener(this)
        mlebu.setOnClickListener(this)
    }



    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogin->{
                var email = edEmail.text.toString()
                var password = edPassword.text.toString()
                if (email.isEmpty() || password.isEmpty()){
                    Toast.makeText(this,"Username / Password tidak boleh kosong ",
                        Toast.LENGTH_SHORT).show()
                }else{
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating...")
                    progressDialog.show()
                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                        .addOnCompleteListener {
                            if (!it.isSuccessful) return@addOnCompleteListener
                            progressDialog.hide()
                            Toast.makeText(this, "Berhasil Login",Toast.LENGTH_SHORT).show()
                            val intent = Intent(this,sukses_login::class.java)
                            startActivity(intent)
                            edEmail.setText("")
                            edPassword.setText("")

                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(this,"username /password salah",
                                Toast.LENGTH_SHORT).show()
                        }
                }
            }
            R.id.mlebu->{
                val intent = Intent(this,registrasi::class.java)
                startActivity(intent)
            }
        }
    }


}
