package nugraha.aditya.appx10

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sukses_login.*

class sukses_login : AppCompatActivity() {

    var fbaut= FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sukses_login)

        btnLoguot.setOnClickListener {
            fbaut.signOut()
            finish()
        }
    }
}